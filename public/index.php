<!DOCTYPE html>
<html lang="fr">
<head>
<title>NV2M</title>
<meta name="description" content="">
<?php include 'inc/head.php'; ?>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
</head>
<body id="home">
<?php include 'inc/header_home.php'; ?>
<div id="main">
  <?php include 'inc/slider.php'; ?>
  <div id="intro">
  	<div>
	    <h2>NV2m : DEMOLITION &ndash; RECYCLAGE DE METAUX ET MATERIAUX</h2>
	    <p>La soci&eacute;t&eacute; NV2M sp&eacute;cialis&eacute;e dans la d&eacute;molition de b&acirc;timent, d&eacute;construction, d&eacute;samiantage, recyclage propose ses services dans les Ardennes (08), la Marne (51) et la Meuse (55). Une &eacute;quipe de sp&eacute;cialistes hautement qualifi&eacute;s assure la conduite des chantiers avec un maximum de r&eacute;activit&eacute; et ce dans le strict respect des r&egrave;gles de s&eacute;curit&eacute; environnementales.</p>
	    <p>Le savoir-faire et le s&eacute;rieux de nos &eacute;quipes sur des missions diverses en d&eacute;molitions de b&acirc;timents, nous ont permis de nous &eacute;quiper en cons&eacute;quence pour les diverses phases de travaux que nous entreprenons.</p>
	    <p>Situ&eacute;e &agrave; Vrigne aux Bois entre Charleville-M&eacute;zi&egrave;res et Sedan, la soci&eacute;t&eacute; NV2M intervient aupr&egrave;s des professionnels et des particuliers pour d&rsquo;autres missions : achat et vente de m&eacute;taux, terrassement, transports de d&eacute;chets et location de bennes.</p>
	</div>
  </div>
</div>
<?php include 'inc/footer.php'; ?>
<?php include 'inc/js.php'; ?>
</body>
</html>
