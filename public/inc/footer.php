<section id="prefooter">
	<div id="coordonnees">
		<div id="horaire"><img class="svg" src="assets/img/horaires.svg" alt="Horaires"> <span>Du lundi au vendredi<br>8h00 12h00 - 14h00 18h00</span></div>
		<div id="adresse"><img class="svg" src="assets/img/pin.svg" alt="Adresse"> <span>861, rue d'Ardin<br>08330 VRIGNE AU BOIS</span></div>
		<div id="tel"><img class="svg" src="assets/img/tel.svg" alt="T&eacute;l."> <span>03 24 32 38 44</span></div>
	</div>
</section>
<footer id="footer">
	<p id="agence"><img class="svg" src="assets/img/agence.svg" alt=""/></p>
	<p id="liens">Harum trium sententiarum nulli prorsus assentior. Nec enim illa prima vera est, ut, quem ad modum in se quisque sit, sic in amicum sit animatus. Quam multa enim, quae nostra causa numquam faceremus, facimus causa amicorum! <a href="#">Liens</a> - <a href="#">Liens</a> - <a href="#">Liens</a> - <a href="#">Liens</a> - <a href="#">Liens</a> - <a href="#">Liens</a> - <a href="#">Liens</a> - <a href="#">Liens</a> - <a href="#">Liens</a> - <a href="#">Liens</a> - <a href="#">Liens</a> - <a href="#">Liens</a></p>
</footer>
