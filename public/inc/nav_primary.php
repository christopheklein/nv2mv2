<nav id="activites">
	<ul>
		<li><a href="page.php">D&eacute;molition</a></li>
		<li><a href="page.php">D&eacute;samiantage</a></li>
		<li><a href="page.php">Recyclage de m&eacute;taux</a></li>
		<li><a href="page.php">Terrassement</a></li>
		<li><a href="page.php">Transports</a></li>
		<li><a href="page.php">Location de bennes</a></li>
	</ul>
</nav>
<nav id="primary" role="primary">
	<ul class="flexnav" data-breakpoint="1024">
		<li><a href="index.php"><img src="assets/img/home.svg" class="svg"></a></li>
		<li><a href="page.php">Pr&eacute;sentation</a></li>
		<li id="savoir_faire"><a href="page.php">Savoir-faire</a>
			<ul>
				<li><a href="page.php">D&eacute;molition</a></li>
				<li><a href="page.php">D&eacute;samiantage</a></li>
				<li><a href="page.php">Recyclage de m&eacute;taux</a></li>
				<li><a href="page.php">Terrassement</a></li>
				<li><a href="page.php">Transports</a></li>
				<li><a href="page.php">Location de bennes</a></li>
			</ul>
		</li>
		<li><a href="page.php">Engagements</a></li>
		<li><a href="page.php">Actus</a></li>
		<li><a href="page.php">Photos</a></li>
		<li><a href="page.php">Acc&egrave;s &amp; Contact</a></li>
	</ul>
	<div class="menu-button">Menu</div>
</nav>