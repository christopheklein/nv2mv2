<header id="header">
	<h1 id="logo">
		<a href="index.php"><img src="assets/img/logo.png" alt="NV2M" /></a>
	</h1>
  	<section id="slider_header">
		<div class="slideshow">
			<img src="assets/img/slider/header/1.jpg">
			<img src="assets/img/slider/header/2.jpg">
		</div>
	</section>
</header>
<?php include 'inc/nav_primary.php'; ?>
